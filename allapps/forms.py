from django import forms

from allapps.models import Register, ContactUs


class RegisterForm(forms.ModelForm):

	class Meta:
		model = Register
		fields = ('full_name', 'email', 'phone', 'illness', 'message')

	def __init__(self, *args, **kwargs):
		super(RegisterForm, self).__init__(*args, **kwargs)
		for name, field in self.fields.items():
			field.widget.attrs.update({'class': 'form-control'})


class ContactForm(forms.ModelForm):

	class Meta:
		model = ContactUs
		fields = ('first_name', 'last_name', 'email_address', 'phone_number', 'message')

	def __init__(self, *args, **kwargs):
		super(ContactForm, self).__init__(*args, **kwargs)
		for name, field in self.fields.items():
			field.widget.attrs.update({'class': 'form-control'})
