from allapps.models import AboutUs, Settings, ContactSection


def project(request):
    about_us = AboutUs.objects.first()
    settings = Settings.objects.first()
    contact_sec = ContactSection.objects.first()

    return {
        'about_us': about_us,
        'settings': settings,
        'contact_section': contact_sec,
    }
