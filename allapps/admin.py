from django.contrib import admin
from .models import *
# Register your models here.


@admin.register(Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    # exclude = ('name','feedback_content','company','department')
    list_display = ('name','feedback_content')


@admin.register(AboutUs)
class AboutUsAdmin(admin.ModelAdmin):
    # exclude = ('title','content')
    list_display = ('title','content')


@admin.register(Illness)
class IllnessAdmin(admin.ModelAdmin):
    # exclude = ('title','content')
    list_display = ('name','content')


@admin.register(Doctors)
class DoctorsAdmin(admin.ModelAdmin):
    # exclude = ('title','content')
    list_display = ('name', 'content')


@admin.register(Register)
class RegisterAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'email', 'illness')

    def has_add_permission(self, request, obj=None):
        return False


@admin.register(ContactUs)
class ContactAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'email_address')

    def has_add_permission(self, request, obj=None):
        return False


@admin.register(ContactSection)
class ContactSectionAdmin(admin.ModelAdmin):
    list_display = ('title', )


@admin.register(Gallery)
class GalleryAdmin(admin.ModelAdmin):
    list_display = ('name', 'pub_date')


admin.site.register(Settings)
