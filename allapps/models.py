from django.db import models
from ckeditor.fields import RichTextField
from django.urls import reverse
from allapps.helper import slugify
from django.conf import settings


class Feedback(models.Model):
	name = models.CharField(max_length=1500, verbose_name="Xestenin adı")
	feedback_content = RichTextField(verbose_name="Feedback mətni")
	image = models.FileField(verbose_name="Şəkil (130x130)")
	draft = models.BooleanField(default=True, verbose_name="Saytda yayımlansın?")
	slug = models.SlugField(editable=False, verbose_name="Slug")

	def __str__(self):
		return ('%s') %(self.name)

	class Meta:
		verbose_name="Feedback"
		verbose_name_plural="Feedbacklər"
		ordering=['-id']

	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)
		super(Feedback, self).save(*args, **kwargs)


class AboutUs(models.Model):
	meta_title=models.CharField(max_length=1500,verbose_name="Meta title",null=True,blank=False)
	meta_keywords=models.CharField(max_length=1500,verbose_name="Meta keywords",null=True,blank=True)
	meta_description=models.TextField(max_length=160,verbose_name="Meta description",null=True,blank=False,help_text="Meta descriptionda max. 150 hərf ola bilər!")
	title=models.CharField(max_length=1500,verbose_name="Başlıq")
	content=RichTextField(verbose_name="Mətn")
	image=models.FileField(verbose_name="Şəkil(602x468)")
	slug=models.SlugField(editable=False, verbose_name="Slug")

	def __str__(self):
		return ('%s') %(self.title)

	class Meta:
		verbose_name="Haqqımızda"
		verbose_name_plural="Haqqımızda"

	def save(self, *args, **kwargs):
		self.slug = slugify(self.title)
		super(AboutUs, self).save(*args, **kwargs)


class Settings(models.Model):
	about_text=models.CharField(max_length=1500,verbose_name="Sayt haqqında(SEO mətni)")
	meta_keywords=models.CharField(max_length=1500,verbose_name="Meta Keywords",null=True,blank=True)
	site_title=models.CharField(max_length=1500,verbose_name="Saytın başlığı")
	email=models.CharField(max_length=1500,verbose_name="Email",null=True)
	number=models.CharField(max_length=1500,verbose_name="Nömrə",null=True)
	adress=models.CharField(max_length=1500,verbose_name="Ünvan",null=True)
	logo=models.FileField(verbose_name="logo(163x38)",blank=True)
	favicon=models.FileField(verbose_name="favicon(32x32)",blank=True)
	facebook=models.CharField(max_length=1500,verbose_name="Facebook",blank=True)
	instagram=models.CharField(max_length=1500,verbose_name="İnstagram",blank=True)
	linkedin=models.CharField(max_length=1500,verbose_name="Linkedin",blank=True)
	youtube=models.CharField(max_length=1500,verbose_name="Youtube",blank=True)
	medium=models.CharField(max_length=1500,verbose_name="Medium",blank=True)
	slug=models.SlugField(editable=False,verbose_name="Slug")
	c_meta_title=models.CharField(max_length=1500,verbose_name="Əlaqə Meta title",null=True,blank=False)
	c_meta_keywords=models.CharField(max_length=1500,verbose_name="Əlaqə Meta keywords",null=True,blank=True)
	c_meta_description=models.TextField(max_length=160,verbose_name="Əlaqə Meta description",null=True,blank=False,help_text="Meta descriptionda max. 150 hərf ola bilər!")
	d_meta_description=models.TextField(max_length=160,verbose_name="Data Bloqu Meta description",null=True,blank=False,help_text="Meta descriptionda max. 150 hərf ola bilər!")

	def __str__(self):
		return ('%s') %(self.site_title)

	class Meta:
		verbose_name="Tənzimləmə"
		verbose_name_plural="Tənzimləmələr"
		ordering=['-id']

	def save(self, *args, **kwargs):
		self.slug = slugify(self.site_title)
		super(Settings, self).save(*args, **kwargs)


class Illness(models.Model):
	name=models.CharField(max_length=1500,verbose_name="Xəstəliliyin adı")
	content=RichTextField(verbose_name="Xəstəlik haqqında")
	image=models.FileField(verbose_name="Şəkil(48x49)")
	slug=models.SlugField(editable=False,verbose_name="Slug")

	def __str__(self):
		return ('%s') %(self.name)

	class Meta:
		verbose_name="Xəstəlik"
		verbose_name_plural="Xəstəliklər"

	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)
		super(Illness, self).save(*args, **kwargs)

	def get_absolute_url(self):
		return reverse('allapps:illness_details', kwargs={'slug': self.slug})


class Doctors(models.Model):
	illness = models.ManyToManyField(Illness, verbose_name="Müalicə etdiyi xəstəliklər", related_name="illness")
	name = models.CharField(max_length=1500, verbose_name="Həkimin adı")
	facebook=models.CharField(max_length=1500,verbose_name="Facebook",blank=True, null=True)
	instagram=models.CharField(max_length=1500,verbose_name="İnstagram",blank=True, null=True)
	linkedin=models.CharField(max_length=1500,verbose_name="Linkedin",blank=True, null=True)
	youtube=models.CharField(max_length=1500,verbose_name="Youtube",blank=True, null=True)
	mail = models.CharField(max_length=1500, verbose_name="Email", blank=True, null=True)
	content = RichTextField(verbose_name="Həkim haqqında")
	image = models.FileField(verbose_name="Şəkil")
	slug = models.SlugField(editable=False, verbose_name="Slug")

	def __str__(self):
		return ('%s') %(self.name)

	class Meta:
		verbose_name = "Həkim"
		verbose_name_plural = "Həkimlər"

	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)
		super(Doctors, self).save(*args, **kwargs)


class Register(models.Model):
	full_name = models.CharField(max_length=150, verbose_name='Adı, Soyadı')
	email = models.CharField(max_length=256, verbose_name='Poçt ünvanı')
	phone = models.CharField(max_length=30, verbose_name='mobil nömrə')
	illness = models.CharField(max_length=100, verbose_name='Xəstəliyin adı')
	message = models.CharField(max_length=1000, verbose_name='Xəstəlik Haqqında')
	slug = models.SlugField(editable=False, verbose_name="Slug")

	def __str__(self):
		return self.full_name

	class Meta:
		verbose_name_plural = "Qeydiyyat olunanlar"

	def save(self, *args, **kwargs):
		self.slug = slugify(self.full_name)
		super(Register, self).save(*args, **kwargs)


class ContactUs(models.Model):
	first_name = models.CharField(max_length=50)
	last_name = models.CharField(max_length=50)
	email_address = models.CharField(max_length=256)
	phone_number = models.CharField(max_length=30)
	message = RichTextField()

	def __str__(self):
		return self.first_name

	class Meta:
		verbose_name_plural = 'Bizə Yazanlar'


class ContactSection(models.Model):
	image = models.FileField(verbose_name="Şəkil(360x512)", upload_to='contact_us/section')
	title = models.CharField(max_length=100, verbose_name='Başlıq')
	content = RichTextField(verbose_name='metn')

	class Meta:
		verbose_name_plural = 'Bizimlə Əlaqə Bölməsi'

	def __str__(self):
		return self.title[:20]


class Gallery(models.Model):
	name = models.CharField(max_length=150, verbose_name="Faylin adi")
	image = models.FileField(verbose_name='Media fayl', upload_to="gallery")
	pub_date = models.DateTimeField(auto_now_add=True, verbose_name="Yüklənmə tarixi")

	class Meta:
		verbose_name_plural = "Galereya"

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)
		super(Gallery, self).save(*args, **kwargs)
