from django.conf.urls import url, include
from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView
from .views import *


app_name = 'allapps'
handler404 = '.views.error_view'


urlpatterns = [
    url(r'^$', index_view, name="index"),
    url(r'^elaqe$', contactus_view, name="contact"),
    url(r'^haqqimizda$', aboutus_view, name="aboutus"),
    url(r'^xestelikler$', services_view, name="services"),
    url(r'^(?P<slug>.*)/$', illness_details, name='illness_details'),
    url(r'^gallery$', gallery_view, name='gallery'),
    ]
