from django.shortcuts import render, redirect, get_object_or_404
from django.core.mail import send_mail
from django.contrib import messages

from .models import *
from allapps.forms import RegisterForm, ContactForm


def index_view(request):
	illness_list = Illness.objects.all()[:3]
	doctors = Doctors.objects.all()[:4]
	one_doctor = Doctors.objects.first()
	feedbacks = Feedback.objects.all()[:3]
	if request.method == "POST":
		reg_form = RegisterForm(request.POST or None)
		if reg_form.is_valid():
			full_name = reg_form.cleaned_data['full_name']
			email = reg_form.cleaned_data['email']
			phone = reg_form.cleaned_data['phone']
			illness = reg_form.cleaned_data['illness']
			message = reg_form.cleaned_data['message']
			message = message + "\n\nPasiyent: " + full_name + "\n\nemail: " + email + "\n\nmobil nömrə: " + phone
			try:
				send_mail(illness, message, 'djangonista@yandex.com', ['foryusif@gmail.com'])
				messages.success(request, "Siz uğurla qeydiyyatdan keçdiniz")
				do_save = Register.objects.create(
					full_name=full_name,
					email=email,
					phone=phone,
					illness=illness,
					message=message
				)
			except:
				messages.error(request, 'Xəta baş verdi')
				return redirect("allapps:index")
	else:
		reg_form = RegisterForm()
	return render(request, 'home/home.html', {
		'reg_form': reg_form,
		'illness_list': illness_list,
		'doctors': doctors,
		'feedbacks': feedbacks,
		'one_doctor': one_doctor,
	})


def contactus_view(request):
	if request.method == "POST":
		con_form = ContactForm(request.POST or None)
		if con_form.is_valid():
			first_name = con_form.cleaned_data['first_name']
			last_name = con_form.cleaned_data['last_name']
			email_address = con_form.cleaned_data['email_address']
			phone_number = con_form.cleaned_data['phone_number']
			message = con_form.cleaned_data['message']
			try:
				do_save = ContactUs.objects.create(
					first_name=first_name,
					last_name=last_name,
					email_address=email_address,
					phone_number=phone_number,
					message=message
				)
				do_save.save()
				messages.success(request, "Mesajınız uğurla göndərildi")
			except:
				messages.error(request, "Xəta baş verdi !")
			return redirect('allapps:contact')
	else:
		con_form = ContactForm()
	return render(request, 'contact/contact.html', {'con_form': con_form})


def aboutus_view(request):
	about = AboutUs.objects.first()
	context = {'about': about}
	return render(request, 'aboutus/aboutus.html', context)


def services_view(request):
	illness = Illness.objects.all()
	context = {'illness': illness}
	return render(request, 'service/service.html', context)


def illness_details(request, slug):
	illness = get_object_or_404(Illness, slug=slug)
	return render(request, 'service/illness_details.html', {'illness': illness})


def gallery_view(request):
	gallery = Gallery.objects.all()
	return render(request, 'gallery/gallery.html', {'gallery': gallery})
